import React from "react";
import { AppLoaderProvider } from "./components/AppLoaderProvider";
import { AppNavigation } from "./config/AppNavigation";
import { appRoutes } from "./config/routes";

function App() {
  return (
    <div className="App">
      <AppLoaderProvider>
        <AppNavigation routes={appRoutes} />
      </AppLoaderProvider>
    </div>
  );
}

export default App;
