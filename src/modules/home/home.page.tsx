import React from "react";
import { Box, Text } from "@chakra-ui/react";
import { ProductGrid } from "../product/component/ProductGrid";
import { BrandGrid } from "../product/component/BrandGrid";
import { ProductCard } from "../product/component/ProductCard";
import { BrandCard } from "../product/component/BrandCard";
import { products } from "../../data";
import { SubHeading } from "../../components/Subheading";

export const HomePage = () => {
  return (
    <Box>
      <Box m={4} mb={6}>
        <SubHeading heading="Brands" my={3} />
        <BrandGrid>
          {products.map((product) => (
            <BrandCard key={product.id} product={product} />
          ))}
        </BrandGrid>
      </Box>
      <Box m={4} mb={6}>
        <SubHeading heading="Quick Links" my={3} />
        <ProductGrid>
          {products.map((product) => (
            <ProductCard key={product.id} product={product} />
          ))}
        </ProductGrid>
      </Box>
    </Box>
  );
};

export default HomePage;
