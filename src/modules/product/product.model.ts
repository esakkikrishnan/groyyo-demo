export type ElementType<T extends ReadonlyArray<unknown>> =
  T extends ReadonlyArray<infer ElementType> ? ElementType : never;

export type ProductImageType = {
  id: string;
  src: string;
  alt: string;
};

export type ProductType = {
  id: string;
  name: string;
  currency: string;
  price: number;
  salePrice?: number;
  flag?: string;
  imageUrl: string;
  rating: number;
  ratingCount: number;
  description: string;
  images: ProductImageType[];
};
