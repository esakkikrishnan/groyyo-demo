import { Button, Flex, HStack, Input, useNumberInput } from "@chakra-ui/react";
import React from "react";

export function QuanityAdder() {
  const { getInputProps, getIncrementButtonProps, getDecrementButtonProps } =
    useNumberInput({
      step: 1,
      defaultValue: 1,
      min: 1,
      max: 5,
      precision: 0,
    });

  const inc = getIncrementButtonProps();
  const dec = getDecrementButtonProps();
  const input = getInputProps({ readOnly: true });

  return (
    <Flex
      alignItems="center"
      justifyContent="center"
      border="1px solid lightgray"
      borderRadius="lg"
      p={2}
    >
      <Button {...dec}>-</Button>
      <Input {...input} border="none" textAlign="center" />
      <Button {...inc}>+</Button>
    </Flex>
  );
}
