import {
  AspectRatio,
  Box,
  Button,
  HStack,
  Image,
  Link,
  Skeleton,
  Stack,
  StackProps,
  Text,
  useBreakpointValue,
  useColorModeValue,
} from "@chakra-ui/react";
import * as React from "react";
import { ProductRating } from "./ProductRating";
import { PriceTag } from "./PriceTag";
import { ProductType } from "../product.model";

interface Props {
  product: ProductType;
  rootProps?: StackProps;
}

export const BrandCard = (props: Props) => {
  const { product, rootProps } = props;
  const { name, imageUrl } = product;
  return (
    <Stack spacing={useBreakpointValue({ base: "4", md: "5" })} {...rootProps}>
      <Box position="relative">
        <Box>
          <AspectRatio ratio={1}>
            <Image
              src={imageUrl}
              alt={name}
              draggable="false"
              fallback={<Skeleton />}
              borderRadius={useBreakpointValue({ base: "md", md: "xl" })}
            />
          </AspectRatio>
          <Box position="absolute" width="100%" bottom={6} textAlign="center">
            <Text fontWeight="medium" color="white">
              {name}
            </Text>
          </Box>
        </Box>
      </Box>
    </Stack>
  );
};
