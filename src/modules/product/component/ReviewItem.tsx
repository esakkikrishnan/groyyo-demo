import { Stack, Flex, Text } from "@chakra-ui/react";
import React from "react";
import { ProductRating } from "./ProductRating";

type ReviewItemProps = {
  rating: number;
  title: string;
  review: string;
};
export const ReviewItem = ({ rating, title, review }: ReviewItemProps) => {
  return (
    <Stack>
      <Flex>
        <ProductRating defaultValue={rating} />
        <Text pl={4} fontWeight="bold">
          {title}
        </Text>
      </Flex>
      <Text fontSize="sm">{review}</Text>
    </Stack>
  );
};
