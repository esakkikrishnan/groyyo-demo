import {
  AspectRatio,
  Box,
  Button,
  Flex,
  HStack,
  Image,
  Link,
  Skeleton,
  Stack,
  StackProps,
  Text,
  useBreakpointValue,
  useColorModeValue,
} from "@chakra-ui/react";
import * as React from "react";
import { ProductRating } from "./ProductRating";
import { formatPrice, Price, PriceTag } from "./PriceTag";
import { ProductType } from "../product.model";
import { useHistory } from "react-router-dom";
import { QuanityAdder } from "./QuanityAdder";
import { AddToCartBtn } from "./AddToCartBtn";

interface Props {
  product: ProductType;
  reviewCount: number;
  rootProps?: StackProps;
}

export const ProductDetail = (props: Props) => {
  const { product, reviewCount, rootProps } = props;
  const { name, imageUrl, price, salePrice, rating, id, currency } = product;
  const { push } = useHistory();
  return (
    <Stack spacing={useBreakpointValue({ base: "4", md: "5" })} {...rootProps}>
      <Flex
        flex="0 0 50%"
        flexWrap="wrap-reverse"
        justifyContent="space-between"
      >
        <Stack
          width={useBreakpointValue({ base: "100%", md: "32%" })}
          spacing={4}
        >
          <Flex>
            <ProductRating defaultValue={rating} />
            <Text pl={4}>{reviewCount} Reviews</Text>
          </Flex>
          <Flex flexDir="column">
            <Text fontSize="3xl">{name}</Text>
            <Price isOnSale={!!salePrice} textProps={{ fontSize: "xl" }}>
              {formatPrice(price, { currency })}
            </Price>
          </Flex>
          <Text>
            With a sleek design and a captivating essence, this is a modern
            Classic made for every occasion.
          </Text>
          <Flex columnGap={4} alignItems="center">
            <QuanityAdder />
            <AddToCartBtn />
          </Flex>
        </Stack>
        <Box width={useBreakpointValue({ base: "100%", md: "62%" })} pb={4}>
          <AspectRatio ratio={4 / 3}>
            <Image
              src={imageUrl}
              alt={name}
              draggable="false"
              fallback={<Skeleton />}
              borderRadius={useBreakpointValue({ base: "md", md: "xl" })}
            />
          </AspectRatio>
        </Box>
      </Flex>
    </Stack>
  );
};
