import { Button, Icon } from "@chakra-ui/react";
import React from "react";
import { FiShoppingCart } from "react-icons/fi";

export const AddToCartBtn = () => (
  <Button p={7}>
    <Icon as={FiShoppingCart} mr={4} /> Add to Cart
  </Button>
);
