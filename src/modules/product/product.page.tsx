import React, { useEffect, useState } from "react";
import { Box, Flex, Stack, Text } from "@chakra-ui/react";
import { products } from "../../data";
import { ProductRating } from "./component/ProductRating";
import { ReviewGrid } from "./component/ReviewGrid";
import { ReviewItem } from "./component/ReviewItem";
import { useParams } from "react-router-dom";
import { ProductType } from "./product.model";
import { ProductDetail } from "./component/ProductDetail";
import { ProductSlider } from "./component/ProductSlider";
import { ProductGrid } from "./component/ProductGrid";

const reviews = [
  {
    rating: 4,
    title: "Typhoon Tyre Inflator",
    review:
      "While filling the air in the car tyres of my Toyota ethios car, the product gets extremely hot within 2-3 minutes of usage. It is almost untouchable. The product does not stay stable on ground and it starts moving while filling the air in tyres.",
  },
  {
    rating: 4,
    title: "Typhoon Tyre Inflator",
    review:
      "While filling the air in the car tyres of my Toyota ethios car, the product gets extremely hot within 2-3 minutes of usage. It is almost untouchable. The product does not stay stable on ground and it starts moving while filling the air in tyres.",
  },
  {
    rating: 4,
    title: "Typhoon Tyre Inflator",
    review:
      "While filling the air in the car tyres of my Toyota ethios car, the product gets extremely hot within 2-3 minutes of usage. It is almost untouchable. The product does not stay stable on ground and it starts moving while filling the air in tyres.",
  },
  {
    rating: 4,
    title: "Typhoon Tyre Inflator",
    review:
      "While filling the air in the car tyres of my Toyota ethios car, the product gets extremely hot within 2-3 minutes of usage. It is almost untouchable. The product does not stay stable on ground and it starts moving while filling the air in tyres.",
  },
  {
    rating: 4,
    title: "Typhoon Tyre Inflator",
    review:
      "While filling the air in the car tyres of my Toyota ethios car, the product gets extremely hot within 2-3 minutes of usage. It is almost untouchable. The product does not stay stable on ground and it starts moving while filling the air in tyres.",
  },
  {
    rating: 4,
    title: "Typhoon Tyre Inflator",
    review:
      "While filling the air in the car tyres of my Toyota ethios car, the product gets extremely hot within 2-3 minutes of usage. It is almost untouchable. The product does not stay stable on ground and it starts moving while filling the air in tyres.",
  },
  {
    rating: 4,
    title: "Typhoon Tyre Inflator",
    review:
      "While filling the air in the car tyres of my Toyota ethios car, the product gets extremely hot within 2-3 minutes of usage. It is almost untouchable. The product does not stay stable on ground and it starts moving while filling the air in tyres.",
  },
];

export const ProductPage = () => {
  const { id } = useParams<{ id: string }>();
  const [product, setProduct] = useState<ProductType>();

  useEffect(() => {
    setProduct(products.find((p) => p.id === id));
  }, [id]);

  return (
    <Box>
      {product && (
        <ProductGrid>
          <ProductDetail product={product} reviewCount={12} />
        </ProductGrid>
      )}

      <Box py={8}>
        <Text textStyle="heading" fontSize="2xl" fontWeight="600">
          Customer Review
        </Text>
        <Flex alignItems="center" py={4}>
          <Text fontSize="4xl" fontWeight="bold">
            4.3
          </Text>
          <Stack px={4} spacing={1}>
            <ProductRating defaultValue={4.3} />
            <Text>Based on 12 reviews</Text>
          </Stack>
        </Flex>
        <ReviewGrid>
          {reviews.map((review, index) => (
            <ReviewItem key={index} {...review} />
          ))}
        </ReviewGrid>
      </Box>
      {/* <ProductSlider /> */}
    </Box>
  );
};

export default ProductPage;
