import { extendTheme } from "@chakra-ui/react";

const themeObj = {
  textStyles: {
    label: {
      fontFamily: "Lato, sans-serif",
      fontSize: "0.75rem",
      fontStyle: "normal",
      fontWeight: 400,
      lineHeight: "1rem",
      letterSpacing: "0.3px",
      textAlign: "left",
    },
    caption: {
      fontFamily: "Lato, sans-serif",
      fontSize: "0.75rem",
      fontStyle: "normal",
      fontWeight: 400,
      lineHeight: "1rem",
      letterSpacing: "0.3px",
      textAlign: "left",
    },
    body: {
      fontFamily: "Lato, sans-serif",
      fontSize: "0.875rem",
      fontStyle: "normal",
      fontWeight: 400,
      lineHeight: "1.25rem",
      letterSpacing: "0.26px",
      textAlign: "left",
    },
    subheading: {
      fontFamily: "Lato, sans-serif",
      fontSize: "0.75rem",
      fontStyle: "normal",
      fontWeight: 600,
      lineHeight: "1.125rem",
      letterSpacing: "0.26px",
      textAlign: "left",
    },
    subtitle: {
      fontFamily: "Lato, sans-serif",
      fontSize: "0.6875rem",
      fontStyle: "normal",
      fontWeight: 600,
      lineHeight: "0.6875rem",
      letterSpacing: "0.3px",
      textAlign: "left",
    },
    helper: {
      fontFamily: "Lato, sans-serif",
      fontSize: "0.75rem",
      fontStyle: "normal",
      fontWeight: 400,
      lineHeight: "1rem",
      letter: "0.32px",
      textAlign: "left",
    },
  },
};

export const theme = extendTheme(themeObj);
