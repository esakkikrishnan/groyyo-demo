import { lazy } from "react";
import { RouteProps } from "react-router-dom";

// features
const HomePage = lazy(
  () => import("../modules/home/home.page" /* webpackChunkName: "HomePage" */)
);
const ProductPage = lazy(
  () =>
    import(
      "../modules/product/product.page" /* webpackChunkName: "ProductPage" */
    )
);

// pages
const NotFoundPage = lazy(
  () => import("../components/NotFound" /* webpackChunkName: "HomePage" */)
);
const SomethingWentWrongPage = lazy(
  () =>
    import(
      "../components/SomethingWentWrong" /* webpackChunkName: "HomePage" */
    )
);

export const APP_ROUTES = {
  HOME: "/",
  PRODUCT: "/product/:id",
  SOMETHING_WENT_WRONG: "/something-went-wrong",
};

export const appRoutes: RouteProps[] = [
  {
    component: HomePage,
    path: APP_ROUTES.HOME,
    exact: true,
  },
  {
    component: ProductPage,
    path: APP_ROUTES.PRODUCT,
    exact: true,
  },
  {
    component: SomethingWentWrongPage,
    path: APP_ROUTES.SOMETHING_WENT_WRONG,
    exact: true,
  },

  // NOTE: BOTTTOM NOT FOUND
  {
    component: NotFoundPage,
  },
];
