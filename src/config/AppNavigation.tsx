import React, { ReactElement, Suspense } from "react";

import {
  Route,
  RouteProps,
  BrowserRouter as Router,
  Switch,
} from "react-router-dom";
import { BaseLayout } from "../components/BaseLayout";
import { OverlayLoader } from "../components/OverlayLoader";
import { ApplicationErrorBoundary } from "../components/ApplicationErrorBoundary";

type AppNavigationProps = {
  routes: RouteProps[];
};
export function AppNavigation({ routes }: AppNavigationProps): ReactElement {
  return (
    <Router>
      <Suspense fallback={<OverlayLoader />}>
        <Switch>
          {routes.map((route) => {
            const { component, ...rest } = route;
            const RouteComponent = component as React.ElementType;
            return (
              <Route
                key={
                  Array.isArray(rest.path)
                    ? rest.path[0]
                    : rest.path
                    ? rest.path
                    : "not_found"
                }
                {...rest}
              >
                <BaseLayout>
                  <ApplicationErrorBoundary>
                    <RouteComponent />
                  </ApplicationErrorBoundary>
                </BaseLayout>
              </Route>
            );
          })}
        </Switch>
      </Suspense>
    </Router>
  );
}
