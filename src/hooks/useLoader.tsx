import React, { ReactNode, useState } from "react";
import { OverlayLoader } from "../components/OverlayLoader";
type useLoaderProps = {
  loader: ReactNode;
  showLoader: () => void;
  hideLoader: () => void;
};

export function useLoader(): useLoaderProps {
  const [isLoading, setLoading] = useState(false);
  return {
    loader: isLoading ? <OverlayLoader /> : null,
    showLoader: () => setLoading(true),
    hideLoader: () => setLoading(false),
  };
}
