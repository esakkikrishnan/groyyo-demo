import React, { ReactNode } from "react";
import {
  Box,
  Flex,
  HStack,
  IconButton,
  Button,
  useDisclosure,
  Stack,
  useColorMode,
  useColorModeValue,
} from "@chakra-ui/react";
import { FiMenu, FiX, FiSun, FiMoon } from "react-icons/fi";
import { Link } from "react-router-dom";

const Links = ["Top Products", "Brands"];
const NavLinks = [
  {
    title: "Top Products",
    link: "/#top-products",
  },
  {
    title: "Brands",
    link: "/#brands",
  },
];

const NavLink = ({ title, link }: { title: string; link: string }) => (
  <Link to={link}>
    <Box
      px={2}
      py={1}
      rounded={"md"}
      _hover={{
        textDecoration: "none",
        bg: useColorModeValue("gray.200", "gray.700"),
      }}
    >
      {title}
    </Box>
  </Link>
);

export function Navbar() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { colorMode, toggleColorMode } = useColorMode();

  return (
    <>
      <Box
        bg={useColorModeValue("gray.100", "gray.900")}
        px={4}
        position="sticky"
        top={0}
        left={0}
        zIndex={1000}
      >
        <Flex h={16} alignItems={"center"} justifyContent={"space-between"}>
          <IconButton
            d="flex"
            justifyContent="center"
            alignItems="center"
            size={"md"}
            icon={isOpen ? <FiX /> : <FiMenu />}
            aria-label={"Open Menu"}
            display={{ md: "none" }}
            onClick={isOpen ? onClose : onOpen}
          />
          <HStack spacing={8} alignItems={"center"}>
            <Box>Groyyo</Box>
            <HStack
              as={"nav"}
              spacing={4}
              display={{ base: "none", md: "flex" }}
            >
              {NavLinks.map((link, index) => (
                <NavLink key={index} title={link.title} link={link.link} />
              ))}
            </HStack>
          </HStack>
          <Flex alignItems={"center"}>
            <Button onClick={toggleColorMode}>
              {colorMode === "light" ? <FiMoon /> : <FiSun />}
            </Button>
          </Flex>
        </Flex>

        {isOpen ? (
          <Box pb={4} display={{ md: "none" }}>
            <Stack as={"nav"} spacing={4}>
              {NavLinks.map((link, index) => (
                <NavLink key={index} title={link.title} link={link.link} />
              ))}
            </Stack>
          </Box>
        ) : null}
      </Box>
    </>
  );
}
