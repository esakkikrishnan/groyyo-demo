import { useLoader } from "../hooks/useLoader";
import React, { createContext, FC, ReactNode } from "react";

type AppLoaderContextType = {
  showLoader: () => void;
  hideLoader: () => void;
};

const initialState: AppLoaderContextType = {
  showLoader: () => console.log,
  hideLoader: () => console.log,
};
export const AppLoaderContext =
  createContext<AppLoaderContextType>(initialState);

type AppLoaderProviderType = {
  children: ReactNode;
};
export const AppLoaderProvider: FC<AppLoaderProviderType> = ({
  children,
}: AppLoaderProviderType) => {
  const { loader, showLoader, hideLoader } = useLoader();
  return (
    <AppLoaderContext.Provider value={{ showLoader, hideLoader }}>
      {children}
      {loader}
    </AppLoaderContext.Provider>
  );
};
