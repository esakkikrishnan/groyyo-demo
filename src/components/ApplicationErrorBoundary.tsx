import React, { Component, ErrorInfo } from "react";
import { CommonErrorPage } from "./CommonError";

export class ApplicationErrorBoundary extends Component<
  any,
  { hasError: boolean }
> {
  constructor(props: any) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error: any): { hasError: boolean } {
    console.group("ApplicationErrorBoundary");
    console.error(error);
    console.groupEnd();
    return { hasError: true };
  }

  componentDidCatch(error: Error, errorInfo: ErrorInfo): void {
    console.group("ApplicationErrorBoundary ErrorInfo");
    console.error(error);
    console.error(errorInfo);
    console.groupEnd();
  }

  render() {
    if (this.state.hasError) {
      return (
        <CommonErrorPage
          type="Oops"
          title="Something Went Wrong"
          description="The page you're looking for does not seem to exist"
        />
      );
    }
    return this.props.children;
  }
}
