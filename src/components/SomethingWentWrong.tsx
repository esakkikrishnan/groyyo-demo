import React from "react";
import { CommonErrorPage } from "./CommonError";

const SomethingWentWrong = () => {
  return (
    <CommonErrorPage
      type="Oops"
      title="Something Went Wrong"
      description="The page you're looking for does not seem to exist"
    />
  );
};

export default SomethingWentWrong;
