import React, { ElementType, ReactElement } from "react";
import {
  Box,
  chakra,
  ChakraProps,
  ComponentWithAs,
  Container,
  forwardRef,
} from "@chakra-ui/react";
import { isValidMotionProp, motion, MotionProps } from "framer-motion";

type OverlayLoaderProps = {
  bg?: string;
};
export const OverlayLoader = ({ bg }: OverlayLoaderProps): ReactElement => (
  <Container
    h="100vh"
    d="flex"
    alignItems="center"
    justifyContent="center"
    bg={bg ? bg : "rgba(0, 0, 0, 0.6)"}
    pos="fixed"
    margin="auto"
    top={0}
    maxW="600px"
    zIndex="tooltip"
    // left={0}
  >
    <FramerMotionBox
      as="aside"
      animate={{
        scale: [1, 2, 2, 1, 1],
        rotate: [0, 0, 270, 270, 0],
        borderRadius: ["20%", "20%", "50%", "50%", "20%"],
      }}
      transition={{
        duration: 2,
        ease: "easeInOut",
        times: [0, 0.2, 0.5, 0.8, 1],
        repeat: Infinity,
        repeatType: "loop",
        repeatDelay: 1,
      }}
      padding="2"
      bg="#fff"
      width="16"
      height="16"
      display="flex"
    >
      <Box position="relative" margin="auto">
        <FramerMotionBox
          as="aside"
          animate={{
            scale: [1, 1, 0.5, 2, 1],
            rotate: [90, 90, 180, 180, 270],
            borderRadius: ["20%", "20%", "50%", "50%", "20%"],
          }}
          transition={{
            duration: 2,
            ease: "easeInOut",
            times: [0, 0.2, 0.5, 0.8, 1],
            repeat: Infinity,
            repeatType: "loop",
            repeatDelay: 1,
          }}
          // padding="2"
          bg="#97144D"
          width="8"
          height="8"
        />
      </Box>
    </FramerMotionBox>
  </Container>
);

export type MotionBoxProps = Omit<ChakraProps, keyof MotionProps> &
  MotionProps & {
    as?: ElementType;
  };

export const FramerMotionBox = motion(
  forwardRef<MotionBoxProps, "div">((props, ref) => {
    const chakraProps = Object.fromEntries(
      // do not pass framer props to DOM element
      Object.entries(props).filter(([key]) => !isValidMotionProp(key))
    );
    return <chakra.div ref={ref} {...chakraProps} />;
  })
) as ComponentWithAs<"div", MotionBoxProps>;
