import React from "react";
import { CommonErrorPage } from "./CommonError";

const NotFoundPage = () => {
  return (
    <CommonErrorPage
      type="404"
      title="Page Not Found"
      description="The page you're looking for does not seem to exist"
    />
  );
};

export default NotFoundPage;
