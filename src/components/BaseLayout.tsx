import { Box, Flex } from "@chakra-ui/react";
import React, { ReactChild, ReactChildren, ReactElement } from "react";
import { Footer } from "./Footer";
import { Navbar } from "./Navbar";
type BaseLayoutProps = {
  children: ReactChild | ReactChildren;
};
export const BaseLayout = ({ children }: BaseLayoutProps): ReactElement => (
  <Box d="flex" flexDir="column" height="100vh">
    <Navbar />
    <Box
      // maxW="7xl"
      mx="auto"
      px={{ base: "4", md: "8", lg: "12" }}
      py={{ base: "6", md: "8", lg: "12" }}
    >
      <Flex
        width="100%"
        height="100%"
        justifyContent="center"
        fontFamily="body"
      >
        <Box>{children}</Box>
      </Flex>
    </Box>
    <Footer />
  </Box>
);
