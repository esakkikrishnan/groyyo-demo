import React, { FC } from "react";
import { Text, TextProps } from "@chakra-ui/react";

type SubHeadingProps = {
  heading: string;
} & TextProps;
export const SubHeading: FC<SubHeadingProps> = ({
  heading,
  ...textprops
}: SubHeadingProps) => (
  <Text
    textStyle="subheading"
    textTransform="uppercase"
    {...textprops}
    color="black-100"
  >
    {heading}
  </Text>
);
